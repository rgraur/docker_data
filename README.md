# Data Docker image #

### Base image ###
* official [Ubuntu image](https://registry.hub.docker.com/_/ubuntu/)

### Specifications ###
* volumes: `/opt/data`

### Run ###
* build image: `docker build -t data .`
* create container: `docker run -v /host/db/backup/path:/opt/data:rw --name data`

